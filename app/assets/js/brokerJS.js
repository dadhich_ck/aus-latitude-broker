
const BIRTHDATE_INPUT = '[data-style-field-name="BirthDate"] input';
const BIRTHMONTH_INPUT = '[data-style-field-name="BirthMonth"] input';
const BIRTHYEAR_INPUT = '[data-style-field-name="BirthYear"] input';
const EMAIL_INPUT = '[data-style-field-name="Email_Address"] input';

const DIRECT_DEBIT_URL = "https://assets.latitudefinancial.com/brochures/au/personal-loans-direct-debit-service-agreement.pdf";
let GEM_SITE = '';
let GEM_SITE_SECTION = '';
let GEM_SITE_SUB_SECTION = '';
let GEM_PAGE_TITLE = '';
let GEM_LINK_TEXT = '';
let GEM_LOCATION = '';

const DD = '[data-style-field-name="Valid_to_Date_Medicare__c"] input';
const MM = '[data-style-field-name="valid_to_month_medicare__c"] input';
const YYYY = '[data-style-field-name="valid_to_year_medicare__c"] input';


const INSURANCE_DD = '[data-style-actor-name="BrInsuranceDetails"] [data-style-section-name="insurance_details_section"].MuiGrid-root.section [data-style-field-name="Expire_Date__c"] input';
const INSURANCE_MM = '[data-style-actor-name="BrInsuranceDetails"] [data-style-section-name="insurance_details_section"].MuiGrid-root.section [data-style-field-name="Expire_Month__c"] input';
const INSURANCE_YYYY = '[data-style-actor-name="BrInsuranceDetails"] [data-style-section-name="insurance_details_section"].MuiGrid-root.section [data-style-field-name="Expire_Year__c"] input';


const WARRANTY_DD = '[data-style-actor-name="BrWarrantyDetails"] [data-style-section-name="warranty_details_section"].MuiGrid-root.section [data-style-field-name="Expire_Date__c"] input';
const WARRANTY_MM = '[data-style-actor-name="BrWarrantyDetails"] [data-style-section-name="warranty_details_section"].MuiGrid-root.section [data-style-field-name="Expire_Month__c"] input';
const WARRANTY_YYYY = '[data-style-actor-name="BrWarrantyDetails"] [data-style-section-name="warranty_details_section"].MuiGrid-root.section [data-style-field-name="Expire_Year__c"] input';


const MANUAL_ADDRESS_SECTION = '[data-style-section-name="Manual_Address_Section"].MuiGrid-container';
const POSTAL_MANUAL_ADDRESS_SECTION = '[data-style-section-name="Broker_Postal_Manual_Address_Section"].MuiGrid-container';
const HOME_ADDRESS_INPUT = '[data-style-field-name="Home_Address"] input';
const POSTAL_ADDRESS_INPUT = '[data-style-field-name="Postal_address_search"] input';
const GPO_ADDRESS_SECTION = '[data-style-section-name="Manual_Po_box_Section"].MuiGrid-container';
const GREEN = "Green";

const POPUPCLOSE = '[data-style-action-name="OpenBankAccountList"]';

const I_CONSENT_TO_THE_ABOVE = "I_Consent_To_The_Above";
const I_CONSENT_TO_THE_ABOVE_FIELD_1 = '[data-style-field-name="I_Consent_To_The_Above_rate"]';
const I_CONSENT_TO_THE_ABOVE_FIELD_2 = '[data-style-field-name="I_Consent_To_The_Above_rate2"]';
const I_CONSENT_TO_THE_ABOVE_FIELD_3 = '[data-style-section-name="ConfirmationCheckSectionLast"] > h3';


const I_CONSENT_TO_THE_ABOVE_ONE = "I_Consent_To_The_Above_Identity";
const I_CONSENT_TO_THE_ABOVE_FIELD_ONE = '[data-style-field-name="I_Consent_To_The_Above_Identity"]';

const I_ACKNOWLEDGE_TO_ABOVE = "I_Acknowledge_To_The_Above";
const I_ACKNOWLEDGE_TO_ABOVE_FIELD = '[data-style-field-name="I_Acknowledge_To_The_Above"]';
const DISCLOSURE = '[data-style-id="disclosure"]';
const COLOR_TOGGLE = 'colorToggle';
const I_CONSENT_TO_THE_ABOVE_TWO = "I_Consent_To_The_Above_Higher_Loan";
const I_CONSENT_TO_THE_ABOVE_TWO_FIELD = '[data-style-field-name="I_Consent_To_The_Above_Higher_Loan"]';
const DISCLOSURE_ONE = "I_Request_To_Nominate";
const DISCLOSURE_TWO = "I_Read_And_Understand";
const DISCLOSURE_THREE = "Disclosure_Three";
const DISCLOSURE_FOUR = "Disclosure_Four";
const DISCLOSURE_ONE_FIELD = '[data-style-field-name="I_Request_To_Nominate"]';
const DISCLOSURE_TWO_FIELD = '[data-style-section-name="ConfirmationCheckSectionTwo"]';
const DISCLOSURE_THREE_FIELD = '[data-style-field-name="Disclosure_Three"]';
const DISCLOSURE_FOUR_FIELD = '[data-style-field-name="Disclosure_Four"]';
const AGREE_ONE = '[data-style-field-name="hide1"]';
const AGREE_TWO = '[data-style-field-name="I_Authorise_to_Credit_Report"]';
const AGREE_THREE = '[data-style-field-name="I_Agree_To_The_Above_Conditions"]';
const AGREE_FOUR = '[data-style-field-name="hide2"]';

const SINGLE_BLANK_SPACE = " ";
const STYLE = "style";

const I_AGREE_TEXT = "I_Agree_To_The_Above_Conditions";

const OTP1_INPUT = '[data-style-field-name="VerifyOTP1"] input';
const OTP2_INPUT = '[data-style-field-name="VerifyOTP2"] input';
const OTP3_INPUT = '[data-style-field-name="VerifyOTP3"] input';
const OTP4_INPUT = '[data-style-field-name="VerifyOTP4"] input';

const EMPTAB = '[data-style-id="tab-step-container"][data-style-actor-name="BrMultipleEmploymentTab"] > div > [data-style-id="tabs"]div > div.MuiTabs-root.tab-bar';
const EMPVAL = 'empTab';
const EXPVAL = 'expTab';
const EXPTAB = '[data-style-id="tab-step-container"][data-style-actor-name="BrMultipleExpensesTab"] > div > [data-style-id="tabs"]div > div.MuiTabs-root.tab-bar';
const IDVAL = 'idTab';
const IDTAB = '[data-style-id="tab-step-container"][data-style-actor-name="BrMultipleIdVerificationTab"] > div > [data-style-id="tabs"]div > div.MuiTabs-root.tab-bar';

const ONE = "One";
const WEEKLY = "Weekly";
const FORTNIGHTLY = "Fortnightly";
const YEARLY = "Yearly";
const MONTHLY = "Monthly";
const YES = "Yes";
const DETAILS = "details";
const GENESIS_FREQUENCE = "genesis__Frequency__c";
const GENESIS_AMOUNT = "genesis__Amount__c";
const FORWARD_SLASH = '/';

const WEEKLY_VALUE = 4.33333333333;
const FORTHNIGHTYLY_VALUE = 2.16666666667;
const YEARLY_VALUE = 0.08333333333;
const DAY_LENGTH = 2;
const MIN_DAY = 1;
const MAX_DAY = 32;
const MAX_MONTH = 13;
const MIN_AGE = 18;
const MAX_AGE = 100;
const YEAR_LENGTH = 4;

const MIN_LOAN_AMOUNT = 5000;
const LOWER_MAX_LOAN_AMOUNT = 20000;
const HIGHER_MAX_LOAN_AMOUNT = 200000;
const MIN_BROKER_FEE = 990;
const MAX_BROKER_FEE = 1650;

const wait = (ms) => new Promise(resolve => setTimeout(resolve, ms));

const STREET_ARR = [
    "Street",
    "Road",
    "Avenue",
    "Terrace",
    "Access",
    "Alley",
    "Alleyway",
    "Amble",
    "Anchorage",
    "Approach",
    "Arcade",
    "Artery",
    "Avenue",
    "Basin",
    "Beach",
    "Bend",
    "Block",
    "Boulevard",
    "Brace",
    "Brae",
    "Break",
    "Bridge",
    "Broadway",
    "Brow",
    "Bypass",
    "Byway",
    "Causeway",
    "Centre",
    "Centreway",
    "Chase",
    "Circle",
    "Circlet",
    "Circuit",
    "Circus",
    "Close",
    "Colonnade",
    "Common",
    "Concourse",
    "Copse",
    "Corner",
    "Corso",
    "Court",
    "Courtyard",
    "Cove",
    "Crescent",
    "Crest",
    "Cross",
    "Crossing",
    "Crossroad",
    "Crossway",
    "Cul-de-sac",
    "Cruiseway",
    "Cutting",
    "Dale",
    "Dell",
    "Deviation",
    "Dip",
    "Distributor",
    "Drive",
    "Driveway",
    "Edge",
    "Elbow",
    "End",
    "Entrance",
    "Esplanade",
    "Estate",
    "Expressway",
    "Extension",
    "Fairway",
    "FireTrack",
    "Firetrail",
    "Flat",
    "Follow",
    "Footway",
    "Foreshore",
    "Formation",
    "Freeway",
    "Frontage",
    "Gap",
    "Garden",
    "Gardens",
    "Gate",
    "Gates",
    "Glade",
    "Glen",
    "Grange",
    "Green",
    "Ground",
    "Grove",
    "Gully",
    "Heights",
    "Highroad",
    "Highway",
    "Hill",
    "Interchange",
    "Intersection",
    "Junction",
    "KeyLanding",
    "Lane",
    "Laneway",
    "Lees",
    "Line",
    "Link",
    "Little",
    "Lookout",
    "Loop",
    "Lower",
    "Mall",
    "Meander",
    "Mew",
    "Mews",
    "Motorway",
    "Mount",
    "Nook",
    "Outlook",
    "Parade",
    "Park",
    "Parklands",
    "Parkway",
    "Part",
    "Pass",
    "Path",
    "Pathway",
    "Piazza",
    "Place",
    "Plateau",
    "Plaza",
    "Pocket",
    "Point",
    "Port",
    "Promenade",
    "Quad",
    "Quadrangle",
    "Quadrant",
    "Quay",
    "Quays",
    "Ramble",
    "Ramp",
    "Range",
    "Reach",
    "Reserve",
    "Rest",
    "Retreat",
    "Ride",
    "Ridge",
    "Ridgeway",
    "RightofWay",
    "Ring",
    "Rise",
    "River",
    "Riverway",
    "Riviera",
    "Road",
    "Roads",
    "Roadside",
    "Roadway",
    "Ronde",
    "Rosebowl",
    "Rotary",
    "Round",
    "Route",
    "Row",
    "Rue",
    "Run",
    "ServiceWay",
    "Siding",
    "Slope",
    "Sound",
    "Spur",
    "Square",
    "Stairs",
    "StateHighway",
    "Steps",
    "Strand",
    "Street",
    "Strip",
    "Subway",
    "TURN",
    "Tarn",
    "Terrace",
    "Thoroughfare",
    "Tollway",
    "Top",
    "Tor",
    "Towers",
    "Track",
    "Trail",
    "Trailer",
    "Triangle",
    "Trunkway",
    "Underpass",
    "Upper",
    "Vale",
    "Viaduct",
    "View",
    "Villas",
    "Vista",
    "Wade",
    "Walk",
    "Walkway",
    "Way",
    "Wharf",
    "Wynd",
    "Yard",
];


const getAge = (dateString) => {
    try {
        let today = new Date();
        let birthDate = new Date(dateString);
        let age = today.getFullYear() - birthDate.getFullYear();
        let m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    } catch (e) {
        console.error('MSG:ERROR OCCURED IN GET AGE FUNCTION', e);
    }
}
const sleep = (milliseconds) => {
    try {
        let date = Date.now();
        let currentDate = null;
        do {
            currentDate = Date.now();
        } while (currentDate - date < milliseconds);
    } catch (e) {
        console.error("MSG: ERROR OCCURED IN SLEEP FUNCTION");
    }
}
const confirmationLink = (url) => {
    try {
        if (document.querySelector('span.docOpenIcon') != null) {
            document.querySelector('span.docOpenIcon').onclick = () => {
                window.open(url);

            }
        }
        if (document.querySelector('span.docLinkOpenIcon') != null) {
            document.querySelector('span.docLinkOpenIcon').onclick = () => {
                window.open(url);

            }
        }
        else {

            setTimeout(confirmationLink(url), 2000);
        }

    }
    catch (e) {
        console.error('MSG: ERROR OCCURED IN confirmationLink', e);
    }
}

((portalext, $) => {
    let dglobal = 0;
    let mglobal = 0;
    let yglobal = 0;

    //Direct debit link
    portalext.callLinkEventDirectDebit = (category, action, label, location) => {
        try {
            document.querySelector('span.directDebit').onclick = () => {
                openUrlWithGemId2(DIRECT_DEBIT_URL);
            }
        } catch (e) {
            console.error('MSG: ERROR OCCURED IN CALLLINKEVENTDIRECTDEBIT', e);
        }
    }
    portalext.DLlicenceURL = (url) => {
        try {
            confirmationLink(url);
        } catch (e) {
            console.error('MSG: ERROR OCCURED IN CALLLINKEVENTDIRECTDEBIT', e);
        }
    }
    const openUrlWithGemId2 = (url) => {
        try {
            url = url;
            window.open(url);
            //openUrlWithGemId2(url);
        } catch (e) {
            console.error("MSG: ERROR OCCURED IN OPENURLWITHGEMID2 FUNCTION", e);
        }
    };


    //MonthlyIncome
    portalext.calculateMonthlyIncome = (primaryFreq, primaryAmount, additionalIncome, applicant, json) => {
        let sum = 0;
        let primaryIncVal = 0;
        let additionalTotalInc = 0;
        primaryInc = primaryAmount;
        try {
            if (applicant === ONE) {
                let primaryFreqVal = (primaryFreq === WEEKLY ? WEEKLY_VALUE : (primaryFreq === FORTNIGHTLY ? FORTHNIGHTYLY_VALUE : (primaryFreq === YEARLY ? YEARLY_VALUE : (primaryFreq === MONTHLY ? 1 : 0))));
                if (primaryFreqVal != 0) {
                    primaryIncVal = (primaryFreq === WEEKLY ? WEEKLY_VALUE : (primaryFreq === FORTNIGHTLY ? FORTHNIGHTYLY_VALUE : (primaryFreq === YEARLY ? YEARLY_VALUE : (primaryFreq === MONTHLY ? 1 : 0)))) * primaryInc;
                } else {
                    primaryIncVal = 0;
                }
                //primaryIncVal = (primaryFreq === WEEKLY ? WEEKLY_VALUE : (primaryFreq === FORTNIGHTLY ? FORTHNIGHTYLY_VALUE : (primaryFreq === YEARLY ? YEARLY_VALUE : (primaryFreq === MONTHLY ? 1 : 0)))) * primaryInc;
                if (additionalIncome === YES) {
                    let m = json[DETAILS];
                    let l = json[DETAILS].size;
                    for (let i = 0; i < l; i++) {
                        let additionalFreqSingle = '', additionIncomeSingle = '';
                        let n = m._root.entries[i][1].fields._root.entries.length;
                        let check = m._root.entries[i][1].fields._root.entries;
                        for (let j = 0; j < n; j++) {
                            if (check[j][0] === GENESIS_FREQUENCE) {
                                additionalFreqSingle = check[j][1];
                            }
                            if (check[j][0] === GENESIS_AMOUNT) {
                                additionIncomeSingle = check[j][1];
                            }
                        }
                        if (additionIncomeSingle === '' || additionIncomeSingle === undefined || additionalFreqSingle === '' || additionalFreqSingle === undefined) {
                            continue;
                        } else {
                            let calcultedSingle = (additionalFreqSingle === WEEKLY ? WEEKLY_VALUE : (additionalFreqSingle === FORTNIGHTLY ? FORTHNIGHTYLY_VALUE : (additionalFreqSingle === YEARLY ? YEARLY_VALUE : 1))) * additionIncomeSingle;
                            additionalTotalInc += calcultedSingle;
                        }
                    }
                }
                sum = additionalTotalInc + primaryIncVal;
                return sum;
            }
            return 0;
        } catch (e) {
            console.error("MSG: ERROR OCCURED DURING CALCULATING MONTHLY INCOME");
        }
    }



    //Method to hide empolyment tab 
    portalext.hideEmpTab = (joint, val) => {
        let hideTab;
        try {
            if (EMPVAL === val) {
                hideTab = EMPTAB;
            } else if (EXPVAL === val) {
                hideTab = EXPTAB;
            } else if (IDVAL === val) {
                hideTab = IDTAB;
            }
            if (joint === false) {
                $(hideTab) && $(hideTab).css({ "display": "none" });
            } else {
                $(hideTab) && $(hideTab).css({ "display": "flex" });
            }
        } catch (e) {
            console.error("MSG: ERROR OCCURED IN DATE CHANGE FUNCTION", e);
        }
    }

    //Method to validate day
    portalext.dateChange = () => {
        try {
            let day = $(DD).val();
            dglobal = day;
            if (day.length === DAY_LENGTH && day > 0 && day < MAX_DAY) {
                $(MM) && $(MM).focus();
            }
        } catch (e) {
            console.error("MSG: ERROR OCCURED IN DATE CHANGE FUNCTION", e);
        }
    };

    //Method to shift focus to Month Field in Insurance page
    portalext.insuranceDateChange = () => {
        try {
            let day = $(INSURANCE_DD).val();
            console.log(day + "DAY");
            dglobal = day;
            if (day.length === DAY_LENGTH && day > 0 && day < MAX_DAY) {
                console.log('inside else');
                $(INSURANCE_MM) && $(INSURANCE_MM).focus();
                console.log('after focus');
            }
        } catch (e) {
            console.error("MSG: ERROR OCCURED IN DATE CHANGE FUNCTION", e);
        }
    };


     //Method to shift focus to Month Field in warranty page
     portalext.warrantyDateChange = () => {
        try {
            let day = $(WARRANTY_DD).val();
            console.log(day + "DAY");
            dglobal = day;
            if (day.length === DAY_LENGTH && day > 0 && day < MAX_DAY) {
                console.log('inside else');
                $(WARRANTY_MM) && $(WARRANTY_MM).focus();
                console.log('after focus');
            }
        } catch (e) {
            console.error("MSG: ERROR OCCURED IN DATE CHANGE FUNCTION", e);
        }
    };

    //Method to shift focus to Year Field
    portalext.monthChange = () => {
        try {
            let month = $(MM).val();
            mglobal = month;
            if (month.length === DAY_LENGTH && month > 0 && month < MAX_MONTH) {
                $(YYYY) && $(YYYY).focus();
            }
        } catch (e) {
            console.error("MSG: ERROR OCCURED IN MONTH CHANGE FUNCTION", e);
        }
    };

    //Method to shift focus to Year Field in insurance page
    portalext.insuranceMonthChange = () => {
        try {
            let month = $(INSURANCE_MM).val();
            mglobal = month;
            if (month.length === DAY_LENGTH && month > 0 && month < MAX_MONTH) {
                $(INSURANCE_YYYY) && $(INSURANCE_YYYY).focus();
            }
        } catch (e) {
            console.error("MSG: ERROR OCCURED IN MONTH CHANGE FUNCTION", e);
        }
    };


    //Method to shift focus to Year Field in warranty page
    portalext.warrantyMonthChange = () => {
        try {
            let month = $(WARRANTY_MM).val();
            mglobal = month;
            if (month.length === DAY_LENGTH && month > 0 && month < MAX_MONTH) {
                $(WARRANTY_YYYY) && $(WARRANTY_YYYY).focus();
            }
        } catch (e) {
            console.error("MSG: ERROR OCCURED IN MONTH CHANGE FUNCTION", e);
        }
    };

    //Method to open Same Page URL
    portalext.openSamePageUrl = (url) => {
        try {
            window.location.replace(url);
        } catch (e) {
            console.error("MSG: ERROR OCCURED IN OPEN URL FUNCTION", e);
        }
    };

    //close bank page popup
    portalext.bankPopupClose = () => {
        try {
            $(POPUPCLOSE) && $(POPUPCLOSE).click();
        } catch (e) {
            console.error("MSG: ERROR OCCURED IN BANK POPUP CLOSE");
        }
    };

    //Expire date check
    portalext.expireDate = (year, val) => {
        // try {
        //     yglobal = year;
        //     let today = new Date();
        //     let age = yglobal - today.getFullYear();
        //     let m = mglobal - today.getMonth() - 1;
        //     let d = dglobal - today.getDate();

        //     if (val !== GREEN) {
        //         return (
        //             age > 0 || (age === 0 && m > 0) || (age === 0 && m === 0 && d >= 0)
        //         );
        //     } else {
        //         return age > 0 || (age === 0 && m >= 0);
        //     }
        // } catch (e) {
        //     console.error("MSG: ERROR OCCURED IN VALID DOB FUNCTION", e);
        // }
        try {
            yglobal = year;
            console.log("DD:" + dglobal + " MM:" + mglobal + "YY:" + year);
            if (dglobal === "" || mglobal === "" || year === "") {
              return false;
            }
            let today = new Date();
            let age = yglobal - today.getFullYear();
            let m = mglobal - today.getMonth() - 1;
            let d = dglobal - today.getDate();
            return (
              age > 0 || (age === 0 && m > 0) || (age === 0 && m === 0 && d > 0)
            );
          } catch (e) {
            console.error("MSG: ERROR OCCURED IN VALID DOB FUNCTION", e);
          }
    };

    //Method to shift focus to Month Field
    portalext.checkDate = () => {
        try {
            let day = $(BIRTHDATE_INPUT).val();
            dglobal = day;
            if (day.length === DAY_LENGTH && day >= MIN_DAY && day < MAX_DAY) {
                $(BIRTHMONTH_INPUT) && $(BIRTHMONTH_INPUT).focus();
            }
        } catch (e) {
            console.error('MSG: ERROR OCCURED IN DATE CHANGE FUNCTION', e);
        }
    };

    //Method to shift focus to Year Field
    portalext.checkMonth = () => {
        try {
            let month = $(BIRTHMONTH_INPUT).val();
            mglobal = month;
            if (month.length === DAY_LENGTH && month > 0 && month < MAX_MONTH) {
                $(BIRTHYEAR_INPUT) && $(BIRTHYEAR_INPUT).focus();
            }
        } catch (e) {
            console.error('MSG: ERROR OCCURED IN MONTH CHANGE FUNCTION', e);
        }

    };

    //Method to shift focus to email field
    portalext.yearChange = () => {
        try {
            let year = $(BIRTHYEAR_INPUT).val();
            yglobal = year;
            let stringDate = yglobal + FORWARD_SLASH + mglobal + FORWARD_SLASH + dglobal;
            let age = getAge(stringDate);
            if (age >= MIN_AGE && age <= MAX_AGE && year.length === YEAR_LENGTH) {
                $(EMAIL_INPUT) && $(EMAIL_INPUT).focus();
            }
            else {
                console.error("You must be atleast 18 years old");
            }
        } catch (e) {
            console.error('MSG: ERROR OCCURED IN YEAR CHANGE FUNCTION', e);
        }

    };

    //Method to Validate DOB
    portalext.validDob = (year) => {
        try {
            let stringDate = yglobal + FORWARD_SLASH + mglobal + FORWARD_SLASH + dglobal;
            let age = getAge(stringDate);
            if (age >= MIN_AGE && age <= MAX_AGE && year.length === YEAR_LENGTH) {
                $(EMAIL_INPUT) && $(EMAIL_INPUT).focus();
                return true;
            }
            else {
                return false;
            }
        } catch (e) {
            console.error('MSG: ERROR OCCURED IN VALID DOB FUNCTION', e);
        }

    };

    //Method to hide address
    portalext.hideAddress = (isChecked) => {
        if (isChecked) {
            $(MANUAL_ADDRESS_SECTION) && $(MANUAL_ADDRESS_SECTION).attr("style", "display: flex !important");
            $(HOME_ADDRESS_INPUT) && $(HOME_ADDRESS_INPUT).attr(STYLE, "background:#f6f6f6 !important");
            $(HOME_ADDRESS_INPUT) && $(HOME_ADDRESS_INPUT).attr("readonly", "true");
        }
        else {
            $(MANUAL_ADDRESS_SECTION) && $(MANUAL_ADDRESS_SECTION).attr("style", "display: none !important");
            $(HOME_ADDRESS_INPUT) && $(HOME_ADDRESS_INPUT).attr(STYLE, "background:transparent !important");
            $(HOME_ADDRESS_INPUT) && $(HOME_ADDRESS_INPUT).removeAttr("readonly");
        }

    };

    //Method to hide Postal address using Postal Manual Address Flag
    portalext.postalhideAddress = (isChecked) => {
        if (isChecked) {
            $(POSTAL_MANUAL_ADDRESS_SECTION) && $(POSTAL_MANUAL_ADDRESS_SECTION).attr("style", "display: flex !important");
            $(POSTAL_ADDRESS_INPUT) && $(POSTAL_ADDRESS_INPUT).attr(STYLE, "background:#f6f6f6 !important");
            $(POSTAL_ADDRESS_INPUT) && $(POSTAL_ADDRESS_INPUT).attr("readonly", "true");
        }
        else {
            $(POSTAL_MANUAL_ADDRESS_SECTION) && $(POSTAL_MANUAL_ADDRESS_SECTION).attr("style", "display: none !important");
            $(POSTAL_ADDRESS_INPUT) && $(POSTAL_ADDRESS_INPUT).attr(STYLE, "background:transparent !important");
            $(POSTAL_ADDRESS_INPUT) && $(POSTAL_ADDRESS_INPUT).removeAttr("readonly");
        }

    };


    //Method to hide Postal address using GPO Manual Address Flag
    portalext.gpohideAddress = (isChecked) => {
        if (isChecked) {
            $(GPO_ADDRESS_SECTION) && $(GPO_ADDRESS_SECTION).attr("style", "display: flex !important");
            $(POSTAL_ADDRESS_INPUT) && $(POSTAL_ADDRESS_INPUT).attr(STYLE, "background:#f6f6f6 !important");
            $(POSTAL_ADDRESS_INPUT) && $(POSTAL_ADDRESS_INPUT).attr("readonly", "true");
        }
        else {
            $(GPO_ADDRESS_SECTION) && $(GPO_ADDRESS_SECTION).attr("style", "display: none !important");
            $(POSTAL_ADDRESS_INPUT) && $(POSTAL_ADDRESS_INPUT).attr(STYLE, "background:transparent !important");
            $(POSTAL_ADDRESS_INPUT) && $(POSTAL_ADDRESS_INPUT).removeAttr("readonly");
        }

    };

    portalext.validatePostBoxRegex = (pobox, val) => {
        console.log(pobox);
        console.log(val);
        if (pobox == false || (pobox == true && (val != null && val != ""))) {
            return true;
        } else {
            return false;
        }
    };

    portalext.validatePostBoxPicklistRegex = (pobox, val) => {
        console.log(pobox);
        console.log(val);
        if (pobox == false || (pobox == true && (val != null && val != "" && val != "Please select"))) {
            return true;
        } else {
            return false;
        }
    };


    //street name problem function
    portalext.streetType = (st, st2) => {
        try {
            let typeArr = st.split(/\s+/);
            let index = typeArr.length;
            let res = typeArr[index - 1];
            if (STREET_ARR.includes(res)) {
                return res;
            } else {
                return st2;
            }
        } catch (e) {
            console.error("MSG: ERROR OCCURED IN STREET TYPE FUNCTION", e);
        }
    };

    //Method to get Street name
    portalext.streetName = (st) => {
        try {
            let len = st.lastIndexOf(SINGLE_BLANK_SPACE);
            st = st.trim();
            let res = st.substring(len + 1, st.length);
            if (STREET_ARR.includes(res)) {
                return st.substring(0, st.lastIndexOf(SINGLE_BLANK_SPACE));
            } else {
                return st;
            }
        } catch (e) {
            console.error("MSG: ERROR OCCURED IN STREET TYPE FUNCTION", e);
        }
    };

    //Method to shift focus in OTP Verification
    portalext.otpfieldChange = (otpPlace, otpValue) => {
        try {

            if (otpPlace == 'otp1' && otpValue) {
                $(OTP2_INPUT) && $(OTP2_INPUT).focus();
            }
            else if (otpPlace == 'otp2' && otpValue) {
                $(OTP3_INPUT) && $(OTP3_INPUT).focus();
            }
            else if (otpPlace == 'otp3' && otpValue) {
                $(OTP4_INPUT) && $(OTP4_INPUT).focus();
            }


        } catch (e) {
            console.error("MSG: ERROR OCCURED IN OTP FIELD CHANGE FUNCTION", e);
        }
    };

    //Broker fee vlidation
    portalext.validateFeeOne = (loanAmt, BrFee) => {
        try {
            if (loanAmt >= MIN_LOAN_AMOUNT && loanAmt <= LOWER_MAX_LOAN_AMOUNT && BrFee <= MIN_BROKER_FEE) {
                return true;
            } else if (loanAmt > LOWER_MAX_LOAN_AMOUNT && loanAmt <= HIGHER_MAX_LOAN_AMOUNT) {
                return true;
            }
            else {
                return false;
            }
        } catch (exp) {
            console.error('MSG: ERROR OCCURED IN VALIDATE FEE FUNCTION', exp);
        }

    };

    //Broker fee validation
    portalext.validateFeeTwo = (loanAmt, BrFee) => {
        try {
            if (loanAmt >= MIN_LOAN_AMOUNT && loanAmt <= LOWER_MAX_LOAN_AMOUNT) {
                return true;
            }
            else if (loanAmt > LOWER_MAX_LOAN_AMOUNT && loanAmt <= HIGHER_MAX_LOAN_AMOUNT && BrFee <= MAX_BROKER_FEE) {
                return true;
            } else {
                return false;
            }
        } catch (exp) {
            console.error('MSG: ERROR OCCURED IN VALIDATE FEE FUNCTION', exp);
        }

    };

    //Disclosure color change on click
    portalext.confirmation = (value, ele) => {
        try {
            let confirmationSection;
            let confirmationDisclosure = DISCLOSURE;
            if (ele === I_CONSENT_TO_THE_ABOVE) {
                confirmationSection = I_CONSENT_TO_THE_ABOVE_FIELD_1;
                if (value) {
                    $(confirmationDisclosure).addClass(COLOR_TOGGLE);
                }
                else {
                    $(confirmationDisclosure).removeClass(COLOR_TOGGLE);
                }
            } else if (ele === I_CONSENT_TO_THE_ABOVE_ONE) {
                confirmationSection = I_CONSENT_TO_THE_ABOVE_FIELD_ONE;
            }
            else if (ele === I_ACKNOWLEDGE_TO_ABOVE) {
                confirmationSection = I_ACKNOWLEDGE_TO_ABOVE_FIELD;
                if (value) {
                    $(confirmationDisclosure) && $(confirmationDisclosure).addClass(COLOR_TOGGLE);
                } else {
                    $(confirmationDisclosure) && $(confirmationDisclosure).removeClass(COLOR_TOGGLE);
                }
            }
            else if (ele === I_CONSENT_TO_THE_ABOVE_TWO) {
                confirmationSection = I_CONSENT_TO_THE_ABOVE_TWO_FIELD;
            }
            else if (ele === DISCLOSURE_ONE) {
                confirmationSection = DISCLOSURE_ONE_FIELD;
            }
            else if (ele === DISCLOSURE_TWO) {
                confirmationSection = DISCLOSURE_TWO_FIELD;
            }
            else if (ele === DISCLOSURE_THREE) {
                confirmationSection = DISCLOSURE_THREE_FIELD;
            }
            else if (ele === DISCLOSURE_FOUR) {
                confirmationSection = DISCLOSURE_FOUR_FIELD;
            }
            else if (ele === I_AGREE_TEXT) {
                if (value === true) {
                    $(AGREE_ONE) && $(AGREE_ONE).addClass(COLOR_TOGGLE);
                    $(AGREE_TWO) && $(AGREE_TWO).addClass(COLOR_TOGGLE);
                    $(AGREE_THREE) && $(AGREE_THREE).addClass(COLOR_TOGGLE);
                    $(AGREE_FOUR) && $(AGREE_FOUR).addClass(COLOR_TOGGLE);
                } else {
                    $(AGREE_ONE) && $(AGREE_ONE).removeClass(COLOR_TOGGLE);
                    $(AGREE_TWO) && $(AGREE_TWO).removeClass(COLOR_TOGGLE);
                    $(AGREE_THREE) && $(AGREE_THREE).removeClass(COLOR_TOGGLE);
                    $(AGREE_FOUR) && $(AGREE_FOUR).removeClass(COLOR_TOGGLE);
                }

            }

            if (value) {
                $(confirmationSection) && $(confirmationSection).addClass(COLOR_TOGGLE);
            } else {
                $(confirmationSection) && $(confirmationSection).removeClass(COLOR_TOGGLE);
            }
        } catch (e) {
            console.error('MSG: ERROR OCCURED IN CONFIRMATION FUNCTION', e);
        }

    };

    //to open url
    portalext.openUrl = (url) => {
        try {
            window.open(url);
        } catch (exp) {
            console.error("MSG: ERROR OCCURED IN OPEN URL", exp);
        }

    };

    //to scroll to top
    portalext.scrollToTop = () => {
        try {
            window.scrollTo(0, 0);
        }
        catch (exp) {
            console.error("MSG: ERROR OCCURED IN SCROLL TO TOP", exp);
        }
    };

    //to scroll to bottom
    portalext.scrollToBottom = () => {
        try {
            window.scrollTo(0, 9999);
            moveScrollToTheBottom
        }
        catch (exp) {
            console.error("MSG: ERROR OCCURED IN SCROLL TO BOTTOM", exp);
        }
    };
})((window.portalext = window.portalext || {}), jQuery);