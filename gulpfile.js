import gulpPkg from "gulp";
import uglify from "gulp-uglify";
import dartsass from "sass";
import gulpsass from "gulp-sass";
import autoprefixer from "gulp-autoprefixer";
import rename from "gulp-rename";
import cleancss from "gulp-clean-css";
import gulpSourcemapsPkg from "gulp-sourcemaps";
import { reload, init } from "browser-sync";
const { task, parallel, series, src, dest, watch } = gulpPkg;
const { write } = gulpSourcemapsPkg;
const sass = gulpsass(dartsass);

task("portal-css", () => {
  return src("src/scss/portal-scss/main.scss")
    .pipe(sass({ errLogToConsole: true }))
    .pipe(autoprefixer("last 4 version"))
    .pipe(dest("app/assets/css/portal"))
    .pipe(cleancss({ compatibility: "ie8" }))
    .pipe(rename({ suffix: ".min" }))
    .pipe(write())
    .pipe(dest("app/assets/css/portal"))
    .pipe(reload({ stream: true }));
});

task("lightning-css", () => {
  return src("src/scss/lightning-scss/lightning.scss")
    .pipe(sass({ errLogToConsole: true }))
    .pipe(autoprefixer("last 4 version"))
    .pipe(dest("app/assets/css/lightning"))
    .pipe(cleancss({ compatibility: "ie8" }))
    .pipe(rename({ suffix: ".min" }))
    .pipe(write())
    .pipe(dest("app/assets/css/lightning"))
    .pipe(reload({ stream: true }));
});

task("portal-js", () => {
  return src("src/js/brokerJS.js")
    .pipe(dest("app/assets/js"))
    .pipe(uglify())
    .on("error", (err) => {
      console.log(err.toString());
    })
    .pipe(rename({ suffix: ".min" }))
    .pipe(write())
    .pipe(dest("app/assets/js"))
    .pipe(reload({ stream: true }));
});

task("browser-sync", () => {
  init(null, {
    server: {
      baseDir: "app",
    },
  });
});

task("bs-reload", () => {
  reload();
});

task("watch", () => {
  watch("src/scss/portal-scss/*/*.scss", series("portal-css"));
  watch("src/scss/lightning-scss/*/*.scss", series("lightning-css"));
  watch("src/js/*.js", series("portal-js"));
  watch("app/*.html", series("bs-reload"));
});

task(
  "default",
  parallel("portal-css", "lightning-css", "portal-js", "browser-sync", "watch")
);
