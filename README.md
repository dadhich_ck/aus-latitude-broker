# **Portal SCSS Boilerplate**

A boilerplate for quickly generating css for portal. We use this boilerplate to write SCSS for our CL Portal Projects.

## **Table of Contents**
- [**Portal SCSS Boilerplate**](#portal-scss-boilerplate)
  - [**Table of Contents**](#table-of-contents)
  - [**Getting Started**](#getting-started)
  - [**Commands**](#commands)

## **Getting Started**

Before you start the below steps, make sure you have installed node (**v16.14.0** or above) in your machine. This repo is compataible with **node** version **16.14.0**. If you install any higer version or lower version of node, you have to adjust project's packages versions accordingly.

**1.** Download the repo:

You can also manually download it and follow the next step to get started

**Note:** `rimraf` is the UNIX command rm -rf for *Node.js*. Deep deletion (like `rm -rf`) module for *Node.js* that provides asynchronous deep-deletion of files and directories. The `rimraf` executable is a faster alternative to the `rm -rf` shell command. (`rm` stands for remove, `rf` stands for recursive force.). So, the above `npx rimraf ./.git` command will dlete the initial git from your project, so taht you can add your own *remote* to the project.

Also please make sure your write the README file accordign to your project.


**2.** Install the dependencies:

```bash
npm install
```
**Note:** After you run `npm install` it install a preinstall dependency package called `npm-force-resolutions`. It helps to install the `glob-parent` `6.0.1` version which is a secure version of `glob-parent`. So, you have to again run `npm install`. This is required if you have your own `package-lock.json` file.

**3.** Command to start the server:

```bash
npm run dev

# Running locallly
```
**4.** Init the git repository according to your project:

```bash
git init
git add README.md
git commit -m "init: initiale setup"
git branch -M master
git remote add origin <YOUR_GIT_REPO_URL>
git push -u origin master

```

**5.** Create New Branch:

Create new branch from master and start writing your SCSS in `./src/scss` folder. Also write the Portal js inside `brokerPortalCustomJs.js` file and any lightning page related JS you can write inside `brokerLightningCustomJs.js` file.

**Note:** The SCSS will be auto compiled to CSS and will be save as `style.css` and `style.min.css` in the `./app.assets/css` folder. Also the JS files are save in the `./app/assets/js` folder.


## **Commands**


Install Packages:
```bash
npm install

# It will read the package.json file and install all the dependency packages.
```

Running locally:
```bash
npm run dev

# Running locallly
```